﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ostreciecie.Startup))]
namespace ostreciecie
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
